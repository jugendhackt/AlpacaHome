/*
    This sketch establishes a TCP connection to a "quote of the day" service.
    It sends a "hello" message, and then prints received data.
*/



#include <ESP8266WiFi.h>


#include <ESP8266HTTPClient.h>


#ifndef STASSID
#define STASSID "jugendhackt"
#define STAPSK "jugendhackt23"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

const char* host = "192.168.178.72";
const uint16_t port = 80;

void setupLight() {
 

  // We start by connecting to a WiFi network

  

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  

 
}


void changeLightState(bool state) 
{  
  WiFiClient client;
    HTTPClient http;

    Serial.print("[HTTP] begin...\n");
    
    // configure traged server and url
    String httpString = "http://";
    String SwitchRelayString = "/switch/relay/turn_";
    String server_path = (httpString + host + SwitchRelayString + boolToString(state));
    // invalid operands of types 'const char [8]' and 'char*' to binary 'operator+'
    http.begin(client, server_path);  
   
    Serial.print("[HTTP] POST...\n");
    // start connection and send HTTP header and body
    int httpCode = http.POST("");

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] POST... code: %d\n", httpCode);

      // file found at server
      
    } else {
      Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();


}
String  boolToString(bool Bool)
{
  if (Bool == true)
  {
    return "on";
  }
  if (Bool == false)
  {
    return "off";
  }
  return "";
}




